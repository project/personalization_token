CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers
 
# Introduction

The **Personalization Token** module provides a UI for content creators to create 
new tokens based on user account values. A fallback value is also specified 
for instances where the user has not filled in the account field. 

**Example:** 

If you have added a `first_name` field to the user account, a Personalization 
Token can be created to render the field value or a defined fallback. Once 
created, you can use the standard token format (i.e. 
`[personalization:first_name]`) anywhere tokens are normally allowed. The 
rendered result will be the field value (i.e. `John`) or, if the field is 
empty, the fallback will be used (i.e. `there`).

###CKEditor Plugin

A provided submodule can also be enabled that allows Personalization Tokens to 
be used within CKEditor via a handy Personalization Token button. See the 
submodule readme for further details and setup instructions.


# Requirements

This module requires the following modules:

 * User module (part of core Drupal 8)
 * [Token module](https://www.drupal.org/project/token)
 
# Installation

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/8/extending-drupal-8 for further information.

# Configuration

Once installed, a new menu item is available via `Admin > Content authoring > 
Personalization tokens`. Create and manage your personalization tokens here.

An `Administer personalization tokens` permission, accessed through the 
regular Drupal permissions page, can be further configured to control who can 
manage tokens.

#FAQ

**Q: How do I create a token?**

A: Visit the Personalization Tokens dashboard and click `Add token`. Complete 
the form.

**Q: How do I use a token?**

A: After creating a token you will see the token on the dashboard (i.e. 
`[personalization:username]`). Simply copy that and paste it anywhere tokens 
are normally allowed. The token will also be available to select within the 
token browser.

NOTE: To use the token within CKEditor you must install the Personalization 
Token CKEditor Plugin submodule.

**Q: How do I add more user account fields (i.e. First name)?**

A: new account fields can be created via `Admin > Configuration > People > 
Account settings > Manage fields`.

# Maintainers
 
 * [Hubbs](https://www.drupal.org/u/hubbs) 
 * [Acro Media Inc.](https://www.acromedia.com)
