<?php

namespace Drupal\personalization_token_ckeditor\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\editor\Entity\Editor;
use Drupal\personalization_token\Entity\PersonalizationToken;

/**
 * Defines the "Personalization Token Replacement" CKEditor plugin.
 *
 * NOTE: The plugin ID ('id' key) corresponds to the CKEditor plugin name.
 * It is the first argument of the CKEDITOR.plugins.add() function in the
 * plugin.js file.
 *
 * @CKEditorPlugin(
 *   id = "token",
 *   label = @Translation("Personalization Token Replacement Plugin")
 * )
 */
class PersonalizationTokenReplacement extends CKEditorPluginBase implements CKEditorPluginButtonsInterface {

  /**
   * {@inheritdoc}
   */
  public function getLibraryPath() {
    $path = '/libraries/token-replacement-ckeditor-plugin';

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getLibraryPath() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableTokens() {
    $available_tokens = [];
    foreach (PersonalizationToken::loadMultiple() as $token) {
      $available_tokens[] = [
        $token->label(),
        $token->id(),
      ];
    }

    return $available_tokens;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = [
      'availableTokens' => $this->getAvailableTokens(),
    ];

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = $this->getLibraryPath();

    return [
      'CreateToken' => [
        'image' => $path . '/icons/token.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

}
