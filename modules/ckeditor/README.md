CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 
# Introduction

The Personalization Token CKEditor module provides a plugin for using 
personalization tokens within CKEditor.

# Requirements

This module requires the following modules:

- Personalization Token module
- CKEditor module (part of core Drupal 8)
- [Token Filter module](https://www.drupal.org/project/token_filter)
- [Token Replacement CKEditor Plugin library](https://github.com/Hubbz/token-replacement-ckeditor-plugin)

# Installation

1. Download and place the Token Replacement CKEditor Plugin library within your
Drupal sites libraries directory (see options below). 
2. Then install the module as you would normally install a contributed Drupal 
module. Visit: https://www.drupal.org/docs/8/extending-drupal-8 for further 
information.

### Installing the Token Replacement CKEditor Plugin library

Install manually or via Composer.

#### Option 1: Manual

1. Download the latest release of `Hubbz/token-replacement-ckeditor-plugin` 
from its [GitHub page](https://github.com/Hubbz/token-replacement-ckeditor-plugin).
2. Extract the plugin files and place them in 
`webroot/libraries/token-replacement-ckeditor-plugin/`.

#### Option 2: Composer

1. Add the following to your composer.json:

    ```
    "require": {
      "hubbz/token-replacement-ckeditor-plugin.git": "master"
    }
    ``` 
    
    ```
    "installer-paths": {
      "web/libraries/{$name}": ["type:drupal-library"]
    }
    ```
    
    ```
    "repositories": {
      {
        "type": "package",
        "package": {
          "name": "Hubbz/token-replacement-ckeditor-plugin",
          "version": "master",
          "type": "drupal-library",
          "source": {
            "url": "https://github.com/hubbz/token-replacement-ckeditor-plugin.git",
            "type": "git",
            "reference": "origin/master"
          }
        }
      }
    }
    ```

2. Run `composer install` 

#Configuration

1. Once installed, go to `Admin > Configuration > Content authoring > Text 
formats and editors`.
2. Click `Configure` for any of the editors (i.e. Full HTML) that you want 
the personalization token plugin to be available within.
3. Drag the Personalization Token icon into the active toolbar area.
4. Under 'Enabled filters', check `Replaces global and entity tokens with 
their values`.
5. Under 'Filter processing order', optionally change filter ordering.
6. Under 'Filter settings', optionally check `Replace empty values`.

#FAQ

**Q: How come I can't select any tokens?**

A: Is the plugin library installed and your CKEditor configured? See 
'installation' and 'configuration' instructions above.

**Q: Where can I add new tokens?**

A: Visit the Personalization Tokens dashboard at `Admin > Content authoring > 
Personalization tokens`. If you can't access this page, ensure that you have 
permissions enabled for your user role.

# Maintainers
 
 * [Hubbs](https://www.drupal.org/u/hubbs) 
 * [Acro Media Inc.](https://www.acromedia.com)
