<?php

namespace Drupal\Tests\personalization_token\Functional;

use Drupal\personalization_token\Entity\PersonalizationToken;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the Personalization Token module.
 *
 * @group personalization_token
 *
 * @ingroup personalization_token
 */
class PersonalizationTokenTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['personalization_token'];

  /**
   * The installation profile to use with this test.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * Various functional test of the Personalization Token module.
   *
   * 1) Verify that the Username token entity was created when the module was
   * installed.
   *
   * 2) Verify that permissions are applied to the various defined paths.
   *
   * 3) Verify that we can manage entities through the user interface.
   *
   * 4) Verify that the entity we add can be re-edited.
   *
   * 5) Verify that required links are present on respective paths.
   *
   * 6) Verify that the entity can be deleted.
   */
  public function testConfigEntityExample() {
    $assert = $this->assertSession();

    // 1) Verify that the Username token entity was created when the module was
    // installed.
    $entity = PersonalizationToken::load('username');
    $this->assertNotNull($entity, 'Username was created during installation.');

    // 2) Verify that permissions are applied to the various defined paths.
    // Define some paths. Since the Username entity is defined, we can use it
    // in our management paths.
    $forbidden_paths = [
      '/admin/config/content/personalization_token',
      '/admin/config/content/personalization_token/add',
      '/admin/config/content/personalization_token/username/edit',
      '/admin/config/content/personalization_token/username/delete',
    ];
    // Check each of the paths to make sure we don't have access. At this point
    // we haven't logged in any users, so the client is anonymous.
    foreach ($forbidden_paths as $path) {
      $this->drupalGet($path);
      $assert->statusCodeEquals(403);
    }

    // Create a user with no permissions.
    $noperms_user = $this->drupalCreateUser();
    $this->drupalLogin($noperms_user);
    // Should be the same result for forbidden paths, since the user needs
    // special permissions for these paths.
    foreach ($forbidden_paths as $path) {
      $this->drupalGet($path);
      $assert->statusCodeEquals(403);
    }

    // Create a user who can administer personalization tokens.
    $admin_user = $this->drupalCreateUser(['administer personalization tokens']);
    $this->drupalLogin($admin_user);
    // Forbidden paths aren't forbidden any more.
    foreach ($forbidden_paths as $unforbidden) {
      $this->drupalGet($unforbidden);
      $assert->statusCodeEquals(200);
    }

    // 3) Verify that we can manage entities through the user interface.
    // We still have the admin user logged in, so we'll create, update, and
    // delete an entity.
    // Go to the list page amd create a new entity.
    $this->drupalGet('/admin/config/content/personalization_token');
    $this->clickLink('Add token');
    $entity_machine_name = 'account_email';
    $this->drupalPostForm(
      NULL,
      [
        'id' => $entity_machine_name,
        'label' => 'Account email',
        'description' => 'Email address for the current user.',
        'user_value' => 'mail',
        'fallback_value' => 'you@example.com',
      ],
      'Add token'
    );

    // Try to re-submit the same entity, and verify that we see an error message
    // and not a PHP error.
    $this->drupalPostForm(
      Url::fromRoute('entity.personalization_token.add_form'),
      [
        'id' => $entity_machine_name,
        'label' => 'Account email',
        'description' => 'Email address for the current user.',
        'user_value' => 'mail',
        'fallback_value' => 'you@example.com',
      ],
      'Add token'
    );
    $assert->pageTextContains('The machine-readable name is already in use.');

    // 4) Verify that our personalization token appears when we edit it.
    $this->drupalGet('/admin/config/content/personalization_token/' . $entity_machine_name . '/edit');
    $assert->fieldExists('label');
    $assert->fieldExists('description');
    $assert->fieldExists('user_value');
    $assert->fieldExists('fallback_value');

    // 5) Verify that required links are present on respective paths.
    $this->drupalGet(Url::fromRoute('entity.personalization_token.list'));
    $this->assertLinkByHref('/admin/config/content/personalization_token/add');
    $this->assertLinkByHref('/admin/config/content/personalization_token/' . $entity_machine_name . '/edit');
    $this->assertLinkByHref('/admin/config/content/personalization_token/' . $entity_machine_name . '/delete');

    // Verify links on Edit Token.
    $this->drupalGet('/admin/config/content/personalization_token/' . $entity_machine_name . '/edit');
    $this->assertLinkByHref('/admin/config/content/personalization_token/' . $entity_machine_name . '/delete');

    // Verify links on Delete Token.
    $this->drupalGet('/admin/config/content/personalization_token/' . $entity_machine_name . '/delete');
    // List page will be the destination of the cancel link.
    $cancel_button = $this->xpath(
      '//a[@id="edit-cancel" and contains(@href, :path)]',
      [':path' => '/admin/config/content/personalization_token']
    );
    $this->assertEqual(count($cancel_button), 1, 'Found cancel button linking to list page.');

    // 6) Verify that the entity can be deleted.
    $this->drupalGet('/admin/config/content/personalization_token/' . $entity_machine_name . '/delete');
    $this->assertSession()->buttonExists('Delete token');
    $this->getSession()->getPage()->pressButton('Delete token');
    $this->assertResponse(200);
    $assert->pageTextContains('The Account email token was deleted.');
  }

}
