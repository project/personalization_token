<?php

namespace Drupal\personalization_token\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the personalization token entity.
 *
 * @ingroup personalization_token
 *
 * @ConfigEntityType(
 *   id = "personalization_token",
 *   label = @Translation("Personalization token"),
 *   admin_permission = "administer personalization tokens",
 *   handlers = {
 *     "access" = "Drupal\personalization_token\PersonalizationTokenAccessController",
 *     "list_builder" = "Drupal\personalization_token\Controller\PersonalizationTokenListBuilder",
 *     "form" = {
 *       "add" = "Drupal\personalization_token\Form\PersonalizationTokenAddForm",
 *       "edit" = "Drupal\personalization_token\Form\PersonalizationTokenEditForm",
 *       "delete" = "Drupal\personalization_token\Form\PersonalizationTokenDeleteForm"
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "description" = "description",
 *     "user_value" = "user_value",
 *     "fallback_value" = "fallback_value"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "user_value",
 *     "fallback_value"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/content/personalization_token/add",
 *     "edit-form" = "/admin/config/content/personalization_token/{personalization_token}/edit",
 *     "delete-form" = "/admin/config/content/personalization_token/{personalization_token}/delete"
 *   }
 * )
 */
class PersonalizationToken extends ConfigEntityBase {

  /**
   * The personalization token ID.
   *
   * @var string
   */
  public $id;

  /**
   * The personalization token UUID.
   *
   * @var string
   */
  public $uuid;

  /**
   * The personalization token label.
   *
   * @var string
   */
  public $label;

  /**
   * The personalization token description.
   *
   * @var string
   */
  public $description;

  /**
   * The personalization token selected user value.
   *
   * @var string
   */
  public $user_value;

  /**
   * The personalization token fallback value.
   *
   * @var string
   */
  public $fallback_value;

}
