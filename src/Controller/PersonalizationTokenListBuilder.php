<?php

namespace Drupal\personalization_token\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of personalization token entities.
 *
 * @ingroup personalization_token
 */
class PersonalizationTokenListBuilder extends ConfigEntityListBuilder {

  /**
   * Builds the header row for the entity listing.
   *
   * @return array
   *   A render array structure of header strings.
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['description'] = $this->t('Description');
    $header['user_value'] = $this->t('User value');
    $header['fallback_value'] = $this->t('Fallback value');
    $header['token'] = $this->t('Token');

    return $header + parent::buildHeader();
  }

  /**
   * Builds a row for an entity in the entity listing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to build the row.
   *
   * @return array
   *   A render array of the table row for displaying the entity.
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['description'] = $entity->description;
    $row['user_value'] = $entity->user_value;
    $row['fallback_value'] = $entity->fallback_value;
    $row['token'] = '[personalization:' . $entity->id() . ']';

    return $row + parent::buildRow($entity);
  }

}
