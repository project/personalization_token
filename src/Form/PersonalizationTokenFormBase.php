<?php

namespace Drupal\personalization_token\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PersonalizationTokenFormBase.
 *
 * @ingroup personalization_token
 */
class PersonalizationTokenFormBase extends EntityForm {

  /**
   * Entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * Construct the PersonalizationTokenFormBase.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   An entity query factory for the personalization token entity type.
   */
  public function __construct(EntityStorageInterface $entity_storage) {
    $this->entityStorage = $entity_storage;
  }

  /**
   * Factory method for PersonalizationTokenFormBase.
   */
  public static function create(ContainerInterface $container) {
    $form = new static($container->get('entity_type.manager')->getStorage('personalization_token'));
    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  /**
   * Returns list of user fields for populating buildForm select options.
   *
   * @return array
   *   An array containing list of filtered options.
   */
  public function getUserValueOptions() {
    $user = \Drupal::currentUser()->id();
    $user_fields = User::load($user)->getFields();
    $options = [];

    foreach ($user_fields as $field) {
      $type = $field->getFieldDefinition()->getType();
      $label = $field->getFieldDefinition()->getLabel();
      $name = $field->getName();

      // Only allow specific fields.
      if ($type == 'string' || $name == 'mail') {
        $options[$name] = $label;
      }
    }

    return $options;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   *
   * Builds the entity add/edit form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An associative array containing the personalization token add/edit form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $personalization_token = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('An administrative title for this token.'),
      '#maxlength' => 255,
      '#default_value' => $personalization_token->label(),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Short description'),
      '#description' => $this->t('Optionally enter short description explaining what this token does.'),
      '#maxlength' => 255,
      '#default_value' => $personalization_token->get('description'),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine name'),
      '#default_value' => $personalization_token->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '([^a-z0-9_]+)',
        'error' => 'The machine-readable name must be unique, and can only contain lowercase letters, numbers, and underscores.',
      ],
      '#disabled' => !$personalization_token->isNew(),
    ];

    $form['user_value'] = [
      '#type' => 'select',
      '#title' => $this->t('User value'),
      '#description' => $this->t('Select the user field that this token will use.'),
      '#options' => $this->getUserValueOptions(),
      '#default_value' => $personalization_token->get('user_value'),
      '#required' => TRUE,
    ];

    $form['fallback_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fallback value'),
      '#description' => $this->t('Optionally enter a fallback value to use if no data is found in the user field.'),
      '#maxlength' => 255,
      '#default_value' => $personalization_token->get('fallback_value'),
    ];

    return $form;
  }

  /**
   * Checks for an existing personalization token.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    $query = $this->entityStorage->getQuery();
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->execute();

    return (bool) $result;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::actions().
   *
   * To set the submit button text, we need to override actions().
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An array of supported actions for the current entity form.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save token');

    return $actions;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::validate().
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function validate(array $form, FormStateInterface $form_state) {
    parent::validate($form, $form_state);
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function save(array $form, FormStateInterface $form_state) {
    $personalization_token = $this->getEntity();
    $status = $personalization_token->save();
    $url = $personalization_token->toUrl();

    // Create an edit link.
    $edit_link = Link::fromTextAndUrl($this->t('Edit'), $url)->toString();

    if ($status == SAVED_UPDATED) {
      // If we edited an existing entity...
      $this->messenger()->addMessage($this->t('%label personalization token has been updated.', ['%label' => $personalization_token->label()]));
      $this->logger('contact')->notice('%label personalization token has been updated..', ['%label' => $personalization_token->label(), 'link' => $edit_link]);
    }
    else {
      // If we created a new entity...
      $this->messenger()->addMessage($this->t('%label personalization token has been created.', ['%label' => $personalization_token->label()]));
      $this->logger('contact')->notice('%label personalization token has been created.', ['%label' => $personalization_token->label(), 'link' => $edit_link]);
    }

    // Redirect the user back to the listing route after the save operation.
    $form_state->setRedirect('entity.personalization_token.list');
  }

}
