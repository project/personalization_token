<?php

namespace Drupal\personalization_token\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class PersonalizationTokenAddForm.
 *
 * Provides the add form for the personalization token entity.
 *
 * @ingroup personalization_token
 */
class PersonalizationTokenAddForm extends PersonalizationTokenFormBase {

  /**
   * Returns the actions provided by this form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   An array of supported actions for the current entity form.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Add token');

    return $actions;
  }

}
